typedef int* TS_DESC_1D[2];
typedef int* TS_DESC_2D[2];

void  STAP_Filter(TS_DESC_1D ptrin1,
		          TS_DESC_2D ptrin2,
		          TS_DESC_2D ptrout,int* pt_desc_out,int ntt,int nsa,int ch_dim1,int ch_dim2)
{
int i,j,R,I,k;
	for(i=0;i<ntt;i++)
	{
		for(j=0;j<nsa;j++)
		{
			R=0;
			I=0;
			for(k=0;k<ntt*nsa;k++)
			{
				R += ptrin1[k][0] * ptrin2[i*nsa*ntt*nsa+j*ntt*nsa+k][0] - ptrin1[k][1] * ptrin2[i*nsa*ntt*nsa+j*ntt*nsa+k][1];
				I += ptrin1[k][0] * ptrin2[i*nsa*ntt*nsa+j*ntt*nsa+k][1] + ptrin1[k][1] * ptrin2[i*nsa*ntt*nsa+j*ntt*nsa+k][0];
			}
			ptrout[i*nsa+j][0] = R;
			ptrout[i*nsa+j][1] = I;
		}
	}
	
	pt_desc_out[ch_dim1]= ntt;
	pt_desc_out[ch_dim2] = nsa;
}
