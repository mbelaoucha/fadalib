
typedef int * TS_DESC_2D[2];
typedef int *  TS_DESC_1D;

void STAP_Apply(TS_DESC_2D pt_desc_in1,int*ptrin1[2],int ch_dim1,
		        TS_DESC_2D pt_desc_in2,int*ptrin2[2],
		        TS_DESC_1D pt_desc_out,int*ptrout[2])
{
	int nsa ; //nombre d'antennes
	int ntt;  //nombre d'echantillons pris
	int nrec; //nobre de recurences
	int i;
	int j;
	int k;  //compteurs de boucles
	float R; 
	float I; // variables intermediaires R : Reel , I : Imaginair 
	
/*	Cplfloat *ptrin1;
	Cplfloat *ptrin2;
	Cplfloat *ptrout;
	
	ntt = pt_desc_in1->ch_dim1;
	nsa = pt_desc_in1->ch_dim2;
	nrec = pt_desc_in2->ch_dim1;
	
	ptrin1 = (Cplfloat*) pt_desc_in1->ch_ptr;
	ptrin2 = (Cplfloat*) pt_desc_in2->ch_ptr;
	ptrout = (Cplfloat*) pt_desc_out->ch_ptr;	*/
	
	//Application des poids
	for(i=0;i<nrec-ntt+1;i++)
	{
		R = 0;
		I = 0;
		for(j=0;j<ntt;j++)
		{
			for(k=0;k<nsa;k++)
			{
				R += ptrin1[j*nsa+k][0] * ptrin2[i*nsa+j*nsa+k][0] - ptrin1[j*nsa+k][1] * ptrin2[i*nsa+j*nsa+k][1];
				I += ptrin1[j*nsa+k][0] * ptrin2[i*nsa+j*nsa+k][1] + ptrin1[j*nsa+k][1] * ptrin2[i*nsa+j*nsa+k][0];
			}
		}
		ptrout[i][0] = R;
		ptrout[i][1] = I;
	}
	pt_desc_out[ch_dim1] = nrec-ntt+1;
}
