
#include <stdio.h>
typedef unsigned int  	STD_uint32;
typedef double  	STD_dpfloat;
typedef int 		FIM_img_desc;
typedef int 		STD_bool;
typedef int     	FIM_work_desc;
typedef int 		ERR_mgr;
	int 		STD_TRUE=1;
	void* 		STD_NULL=NULL;
	int		pt_image_result__fullwork___scan__ad;
 	int		GlobalMemory[100000000];
	int		p_in[100000],p_ref[100000];

int MAK_set_work_xyminmax(int,int,int,int,ERR_mgr*);
int abs(int);




STD_bool  fast_correlate(
	const FIM_img_desc  * pt_image_in,int pt_image_in__size_x, int pt_image_in__size_y,
	const FIM_img_desc  * pt_image_ref,
	FIM_img_desc        * pt_image_result,
	ERR_mgr             * pt_emgr,
	int pt_image_result__fullwork__scan__inc__x,int pt_image_result__fullwork__scan__skip__x
	)
{
// #define FUNCTION "fast_correlate"

     FIM_work_desc  wd_ref;
 	
 	STD_uint32  size_x;
 	STD_uint32  size_y;
 	
 	STD_uint32  x, y;
 	
 	int	pt_res ;
 	float  diff_sum;
	int		k;	



	/* Scan positions */
	pt_res =  pt_image_result__fullwork___scan__ad;
	
	for ( y = 0; y < size_y; y ++ )
	{
		for ( x = 0; x < size_x; x ++ )
		{
		  wd_ref=	MAK_set_work_xyminmax(
				x, x + pt_image_in__size_x - 1,
				y, y + pt_image_in__size_y - 1,
				pt_emgr );
				
			diff_sum = 0;
			
// 			FIM_BinaryWorkOp(
// 				pt_image_in->fullwork, & wd_ref,
// 				DFLOAT, DFLOAT, p_in, p_ref,
				k=0;
				while(k<wd_ref)
 				{ 
 					diff_sum += abs(p_in[k] - p_ref[k]);
					k++;
 				}
// 				);

				
			/* Normalisation */
			GlobalMemory[pt_res] = diff_sum / (pt_image_in__size_x * pt_image_in__size_y);
			
			/* Next */
			pt_res += pt_image_result__fullwork__scan__inc__x;
		}
		
		/* Next */
		pt_res += pt_image_result__fullwork__scan__skip__x;
	}


    return (STD_TRUE);
    
}
