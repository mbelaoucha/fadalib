typedef float float32;
int Deriche_CalcCoef(int,int);
void Deriche_Lisseur_C99(int h, int w, float32 X[h][w], float32 Y[h][w], float32 alpha, int i0, int i1, int j0, int j1)
/* ----------------------------------------------------------------------------------------------------------------- */
{
      int b0,a2,a1,i,j;

 b0=   Deriche_CalcCoef(alpha, 1);
 a1= Deriche_CalcCoef(alpha, 1);   
 a2=Deriche_CalcCoef(alpha, 1);
    for(i=i0; i<i1; i++) {
        for(j=j0+2; j<=j1; j++) {
            Y[i][j] = b0 * X[i][j] + a1 * Y[i][j-1] + a2 * Y[i][j-2];
        }
    }
}
