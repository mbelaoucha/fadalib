
extern int *X,*sv2,*sv33,*sv18,sv19,*sv23,*sv38,*sv13,*sv26,*sv39;
extern int *n1,*n2,*n3,*n4,*n5,*n6,*n7,*n8,*n9,*n10,*n11,*n12,*n13,*n14,*n15,*n16,*n17,*n18,*n19,*n20,*n21,*n22,*n23,*n24,*n25,*n26,*n27,*n28,*n29;
extern int m1,m2,m3,m4,m5,m6,m7,m8;

int  elliptic(int N){
int k;
for(k=1;k<=N;k++){
		n1[k] = X[k] + sv2[k];			//s0	
		n2[k] = sv33[k-1] + sv39[k-1];		//s1
		n3[k] = n1[k] + sv13[k-1];		//s2
		n4[k] = n3[k] + sv26[k-1];		//s3
		n5[k] = n4[k] + n2[k];			//s4
		n6[k] = n5[k] * m1;			//s5
		n7[k] = n5[k] * m2;			//s6
		n8[k] = n3[k] + n6[k];			//s7
		n9[k] = n7[k] + n2[k];			//s8
		n10[k] = n3[k] + n8[k];		//s9
		n11[k] = n8[k] + n5[k];		//s10
		n12[k] = n2[k] + n9[k];		//s11
		n13[k] = n10[k] * m3;			//s12
		n14[k] = n12[k] * m4;			//s13
		n15[k] = n1[k] + n13[k];		//s14
		n16[k] = n14[k] + sv39[k - 1];		//s15
		n17[k] = n1[k] + n15[k];		//s16
		n18[k] = n15[k] + n8[k];		//s17
		n19[k] = n9[k] + n16[k];		//s18
		n20[k] = n16[k] + sv39[k - 1];		//s19
		n21[k] = n17[k] * m5;			//s20
		n22[k] = n18[k] + sv18[k - 1];		//s21
		n23[k] = sv38[k-1] + n19[k];		//s22
		n24[k] = n20[k] *m6;			//s23
		n25[k] = X[k] + n21[k];		//s24
		n26[k] = n22[k] * m7;			//s25
		n27[k] = n23[k] * m8;			//s26
		n28[k] = n26[k] + sv18[k-1];		//s27
		n29[k] = n27[k] + sv38[k-1];		//s28
		sv2[k] = n25[k] + n15[k];		//s29
 		sv13[k] = n22[k] + n28[k];		//s30
		sv18[k] = n28[k];			//s31
		sv26[k] = n9[k] + n11[k];		//s32
		sv38[k] = n29[k];			//s33
		sv33[k] = n23[k] + n29[k];		//s34
		sv39[k] = n16[k] + n24[k];		//s35
	}
return(0);
}
