typedef float float32;
extern int f;
void Deriche_Derivateur_C99(int h, int w, float32 X[h][w], float32 Y[h][w], int i0, int i1, int j0, int j1)
{
    int i;
    int j;

    for(i=i0; i<=i1-1; i++) {
        for(j=j0; j<=j1-1; j++) {
            Y[i][j] = (X[i][j] - X[i][j+1] + X[i+1][j] - X[i+1][j+1])/2*f;
            //Y[i][j] = -X[i][j];
        }
    }
}
