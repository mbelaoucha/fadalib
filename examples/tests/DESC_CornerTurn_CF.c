typedef float Cplfloat[2];

void DESC_CornerTurn_CF (Cplfloat * ptr_in, int nbDimIN, int *sauts_in,	// [nbDimIN]
								 int *Org,	// [nbDimIN]
								 int *length,	// [nbDimOUT]
								 int *no_dim,	// [nbDimOUT]
								 int *ampl,	// [nbDimOUT]
								 int nbDimOUT, Cplfloat * ptr_out)
{

 	int sauts_out[nbDimOUT];
 
 	int NbDataTotal;
 	int n, L, M, ORG;
 	int ad_read, ad_write;

	L = 1;
	for (n = 0; n < nbDimOUT; n++) {
		sauts_out[nbDimOUT - 1 - n] = L;
		L = L * length[nbDimOUT - 1 - n];
		}
	NbDataTotal = L;

	ORG = 0;
	for (n = 0; n < nbDimIN; n++)
		ORG += Org[n] * sauts_in[n];

	ad_write=0;
	while(ad_write < NbDataTotal){
	 	for (ad_write = 0; ad_write < NbDataTotal; ad_write++) {
			ad_read = ORG;
			L = ad_write;
			for (n = 0; n < nbDimOUT; n++) {
				M = L / sauts_out[n];
				ad_read += M * ampl[n] * sauts_in[no_dim[n]];
				L = L - M * sauts_out[n];
				}
			}
	ptr_out[ad_write][0] = ptr_in[ad_read][0];
	ptr_out[ad_write][1]  = ptr_in[ad_read][1] ;
			
	ad_write++;
	}
}

