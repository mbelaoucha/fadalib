typedef int int32;
void sobel_C99_I32(int h, int w, int32 X[h][w], int i0, int i1, int j0, int j1, int32 Gx[h][w], int32 Gy[h][w])
/* --------------------------------------------------------------------------------------------------------- */
{
/*
* [-1 0 +1] [-1 -2 -1]
* [-2 0 +2] [ 0  0  0]
* [-1 0 +1] [+1 +2 +1]
    */
    
int i,j;
    
    for(i=i0+1; i<=i1-1; i++) {
        for(j=j0+1; j<=j1-1; j++) {
            
            Gx[i][j]  = (X[i-1][j+1] + 2 * X[i][j+1] + X[i+1][j+1]);
            Gx[i][j] -= (X[i-1][j-1] + 2 * X[i][j-1] + X[i+1][j-1]);
            Gx[i][j] /= 4;
            
            Gy[i][j]  = (X[i+1][j-1] + 2 * X[i+1][j] + X[i+1][j+1]);
            Gy[i][j] -= (X[i-1][j-1] + 2 * X[i-1][j] + X[i-1][j+1]);
            Gy[i][j] /= 4;
        }
    }
    // voir fonction appelante
    //makeBorder1_2D_I32(Y, i0, i1, j0, j1); // post duplication ->[i0..i1]x[j0..j1]
}
