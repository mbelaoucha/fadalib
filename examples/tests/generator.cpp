
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <sstream>
using namespace std;

string generate_while_nest(string,int);
string generate_single_if(string);
int
main(int argc, char* argv[]){
if(argc > 1){
	int nb_nest;
	sscanf(argv[1],"%d",&nb_nest);
	string result=generate_while_nest("",nb_nest);
	result="int\nmain(){\n"+result+"\n}";
	cout<<result;
	exit(EXIT_SUCCESS);
	}
else{
	cout<<"a positive number is expected\n";	
	exit(EXIT_FAILURE);
	}
}

/////////////..........................................................
//
//

string
generate_while_nest(string indent, int id_nest){
if(id_nest<=0)
	return generate_single_if(indent+"   ");
ostringstream lc;
ostringstream cond;
lc<<"lc_"<<id_nest;
cond<<"cond_"<<id_nest<<"("<<lc.str()<<")";
string result;
result="\n"+indent+"while("+cond.str()+"){";
result+=generate_while_nest(indent+"   ",id_nest-1);
result+="\n"+indent+"   "+lc.str()+"++;\n"+indent+"   }" ;
return result;
}

string
generate_single_if(string indent){
string result="\n"+indent+"if(cond(variable)<0)";
result+="\n"+indent+"   T=1;";
result+="\n"+indent+"else";
result+="\n"+indent+"   T=2;";
result+="\n"+indent+"result=T;";
return result;
} 
