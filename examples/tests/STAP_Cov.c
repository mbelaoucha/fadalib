typedef int TS_DESC_2D;
typedef int TS_DESC_3D[1000];

void STAP_Cov(TS_DESC_2D pt_desc_in,int* ptrin[2],int nrec, int nsa,
			  int ntt, int * ptrout[2],//nombre d'echantillons pris
			  TS_DESC_3D pt_desc_out,int ch_dim1,int ch_dim2,int ch_dim3)
{
	int R,I,i,j,k;
	R = 0;
	I = 0;
	// Calcul des matrices de covariance
	for(i=0;i<nrec-ntt+1;i++)
	{
		for(j=0;j<nsa*ntt;j++)
		{
			for(k=0;k<nsa*ntt;k++)
			{
				R = ptrin[i*nsa+j][0] * ptrin[i*nsa+k][0] + ptrin[i*nsa+j][1] * ptrin[i*nsa+k][1];
				I = - ptrin[i*nsa+j][0] * ptrin[i*nsa+k][1] + ptrin[i*nsa+j][1] * ptrin[i*nsa+k][0];
				ptrout[i*nsa*ntt*nsa*ntt+j*nsa*ntt+k][0] = R;
				ptrout[i*nsa*ntt*nsa*ntt+j*nsa*ntt+k][1] = I;
			}
		}
	}
	pt_desc_out[ch_dim1] = nrec-ntt+1;
	pt_desc_out[ch_dim2] = nsa*ntt;
	pt_desc_out[ch_dim3] = nsa*ntt;
}
