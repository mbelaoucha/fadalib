
typedef int imgpel[2][1000][1000];
extern int ref1_line[1000][1000];
extern int ref2_line[1000][1000];
extern int mcost;
extern int byte_abs[1000];
extern int right_shift(int,int);

int computeBiPredSAD1(imgpel src_pic,
                      int blocksize_y,
                      int blocksize_x,
                      int min_mcost,
                      int cand_x1, int cand_y1,
                      int cand_x2, int cand_y2)
{
 int y,x4;
 int bi_diff;
int  ChromaMEEnable;
 int k;
 
  for (y = 0; y < blocksize_y; y++)
  {
    for (x4 = 0; x4 < blocksize_x; x4++)
    {
      
      bi_diff = src_pic[0][y][x4*4] - ((ref1_line[y][x4*4] + ref2_line[y][x4*4] + 1)*2);
      mcost += byte_abs[bi_diff];
      
      bi_diff = src_pic[0][y][x4*4] - ((ref1_line[y][x4*4] + ref2_line[y][x4*4] + 1)*2);
      mcost += byte_abs[bi_diff];
      
      bi_diff = src_pic[0][y][x4*4] - ((ref1_line[y][x4*4] + ref2_line[y][x4*4] + 1)*2);
      mcost += byte_abs[bi_diff];
      
      bi_diff = src_pic[0][y][x4*4] - ((ref1_line[y][x4*4] + ref2_line[y][x4*4] + 1)*2);
      src_pic[0][y][x4*4]++;
      ref1_line[y][x4*4]++;
      ref2_line[y][x4*4]++;
      mcost += byte_abs[bi_diff];
    }
    if (mcost >= min_mcost) return(mcost);
  }

  if ( ChromaMEEnable ) {
    // calculate chroma conribution to motion compensation error

    for (k=0; k<2; k++)
    {
      
      for (y=0; y<blocksize_y; y++)
      {
        for (x4 = 0; x4 < blocksize_x; x4++)
        {
          bi_diff = src_pic[k][y][x4] - right_shift((ref1_line[y][x4*4] + ref2_line[y][x4] + 1),1);
          mcost += byte_abs[bi_diff];
          bi_diff = src_pic[k][y][x4] - right_shift((ref1_line[y][x4*4] + ref2_line[y][x4] + 1),1);
          mcost += byte_abs[bi_diff];
        }

        if (mcost >= min_mcost) 
	    return (mcost);
      }
    }
  }
  return (mcost);
}
