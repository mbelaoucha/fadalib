
typedef int* TS_DESC_2D;
typedef int* TS_DESC_1D;
typedef float Cplfloat[2];

void Filtre_Dir (TS_DESC_2D  pt_desc_in1, TS_DESC_1D  pt_desc_in2, TS_DESC_1D  pt_desc_out,int ch_dim1)
{
	int i, j;						  // compteurs de boucles
	int na;							  // antennes
	int nv;							  // voies
	float R, I;						  // variables intermediaires R : Reel , I : Imaginair 

	Cplfloat *ptrin1, *ptrin2;
	Cplfloat *ptrout;

//	na = pt_desc_in1->ch_dim2;
//	nv = pt_desc_in1->ch_dim1;

//	ptrin1 = (Cplfloat *) pt_desc_in1->ch_ptr;
	//ptrin2 = (Cplfloat *) pt_desc_in2->ch_ptr;
//	ptrout = (Cplfloat *) pt_desc_out->ch_ptr;

	//Application des Stering Vectors
	for (i = 0; i < nv; i++) {
		R = 0;
		I = 0;
		for (j = 0; j < na; j++) {
			R += ptrin1[i * na + j][0] * ptrin2[i * na + j][0] - ptrin1[i * na + j][1] * ptrin2[i * na + j][1];
			I += ptrin1[i * na + j][0] * ptrin2[i * na + j][1] + ptrin1[i * na + j][1] * ptrin2[i * na + j][0];
		}
		ptrout[i][0] = R;
		ptrout[i][1] = I;
	}
	pt_desc_out[ch_dim1] = nv;

}
