
extern double atan2(double,double);
extern double sin(double);
extern double cos(double);

void yacobi(float maxcyc,int n,float a[1000][1000]){
int count,p,q,i;
double theta,app,apq,aqq,csq,ssq,aip,aiq,cs,c,s;
for( count=1; count<=maxcyc;count++){
	for(p=1;p<=n-1;p++){
		for(q=p+1;q<=n;q++){
			app = a[p][p];
			apq = a[p][q];
			aqq = a[q][q];
			theta = atan2(2*apq,app-aqq)/2;
			c = cos(theta);
			s = sin(theta);
			csq = c*c;
			ssq = s*s;
			cs = c*s;
			a[p][p] = ((csq*app)+((2*cs)*apq))+(ssq*aqq);
			a[q][q] = ((ssq*app)-((2*cs)*apq))+(csq*aqq);
			a[p][q] = 0;
			a[q][p] = 0;
			for(i=1;i<=p-1;i++){
				aip = a[i][p];
				aiq = a[i][q];
				a[i][p] = (c*aip)+(s*aiq);
				a[p][i] = a[i][p];
				a[i][q] = ((-s)*aip)+(c*aiq);
				a[q][i] = a[i][q];
				}
			for(i=p+1;i<q-1;i++){
				aip = a[i][p];
				aiq = a[i][q];
				a[i][p] = (c*aip)+(s*aiq);
				a[p][i] = a[i][p];
          			a[i][q] = ((-s)*aip)+(c*aiq);
				a[q][i] = a[i][q];
				}
			for(i=q+1;i<=n;i++){
				aip = a[i][p];
				aiq = a[i][q];
				a[i][p] = (c*aip)+(s*aiq);
				a[p][i] = a[i][p];
				a[i][q] = ((-s)*aip)+(c*aiq);
				a[q][i] = a[i][q];
				}
			}
		}
	}
}
