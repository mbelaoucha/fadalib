void
pafsor(int N,int MAXITS,int a,int b,int c,int d, int e){
int j,l,w;
int f[N][N],u[N][N];
for( j= 1; j<= N; j++){
	for(l= 1; l<=N; l++){
		f[j][l] = 0;
		u[j][l] = 0;
		}
	}

for(   w = 2; w<=MAXITS; w++){
	for(   j= 2; j<= N-1; j++){
		for(   l= 2; l<= N-1; l++){
			u[j][l] = (a * u[j+1][ l] + b * u[j-1][ l] +
				c * u[j][ l+1] + d * u[j][ l-1] 
				- f[j][l] ) / e;
			}
		}
	}
}
